# Números romanos e seus respectivos valores.
symbols_and_values = {
    "I": 1,
    "IV": 4,
    "V": 5,
    "IX": 9,
    "X": 10,
    "XL": 40,
    "L": 50,
    "XC": 90,
    "C": 100,
    "CD": 400,
    "D": 500,
    "CM": 900,
    "M": 1000
}

class Solution:

    def convert_roman_to_decimal(self, roman_number: str) -> int | str:
        """Converte um número romano para decimal."""
        # Representa o valor do número romano.
        value = 0
        # Representa quantos números romanos já foram convertidos.
        count = 0
        # A Cópia do número romano.
        roman_copy = roman_number
        # Pega primeiro os números romanos com dois símbolos.
        try:
            for i in range(len(roman_number) - 1):
                if (roman_number[i] + roman_number[i + 1]) in symbols_and_values:
                    # Adiciona o valor do número romano ao total.
                    value += symbols_and_values[roman_number[i] + roman_number[i + 1]]
                    # Remove o número romano, da cópia, com os dois símbolos correspondente.
                    roman_copy = roman_copy[:i - count] + roman_copy[i + 2 - count:]
                    # Incrementa a quantiade de números romanos já convertidos.
                    count += 2
            # Gera uma lista com os valores convertidos para cada símbolo restante correspondente.
            # E então, retorna o valor do número romano.
            return value + sum([symbols_and_values[symbol.upper()] for symbol in roman_copy])
        except KeyError:
            return "A entrada inserida não possui caracteres válidos."

if __name__ == "__main__":
    # Exemplo de entrada:
    # mcmxciv

    # Entrada única.
    print(Solution().convert_roman_to_decimal(input("> ").upper()))

    # Múltiplas entradas (Testcases).
    # for t in range(list(map(int, input("Testcases: ").split(" ")))[0]):
    #     print(f"#{t}: {Solution().convert_roman_to_decimal(input('> ').upper())}")