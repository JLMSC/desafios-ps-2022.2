# **Desafios PS 2022.2**

## **Feito por**
**Joan Lucas Marques de Sousa Chaves**

## **Linguagem Utilizada**
**Python 3.10.4**

## **Observações**
Todos os código dispões de entradas únicas e múltiplas (*testcases*),
apenas o "Poker Espacial" não possui, pois o mesmo é baseado em um cenário aleatório.

Todos os módulos utilizados são padrões da linguagem Python, 

Sobre os modulos utilizados:

1. "***choice***" do módulo "***random***", escolhe um elemento aleatório de um sequência.
2. "***sample***" do módulo "***random***", retorna uma lista com '*n*' elementos selecionados aleatóriamente.
3. "***shuffle***" do módulo "***random***", muda as posições dos elementos de uma lista de forma aleatória.
4. "***combinations***" do módulo "***itertools***', retorna todas as possíveis combinações de '*n*' elementos de uma lista.
5. "***dataclass***" do módulo "***dataclass***", usado para diminuir criação de métodos construtores e toString.
6. "***field***" do módulo "***dataclass***", usado para definir valores padrões, os quais são inicializados no método construtor da função, gerado pelo próprio dataclass e, também, para definir o tipo de determinada variável, além das demais opções as quais permitem maior controle sobre as variáveis de uma classe.