from random import choice

class Solution:

    def _generate_constellation(self, stars_count: int, star_a: int) -> list[list[int]]:
        """Gera uma matriz de '1s' e '0s' com 'stars_count^2' elementos."""
        if 4 <= stars_count <= 8:
            # Gera uma matriz 'stars_count^2' com todos os elementos zerados.
            constellation = [[0 for _ in range(stars_count)] for _ in range(stars_count)]

            # Itera sobre os elementos acima da diagonal principal.
            for i in range(stars_count):
                for j in range(i + 1, stars_count):
                    # Faz um arco não-direcionado entre as duas estrelas, com o valor de '0' ou '1'.
                    constellation[i][j] = constellation[j][i] = choice([0, 1])

            # Retorna a matriz gerada.
            return constellation
        raise ValueError("A quantidade de estrelas deve ser no mínimo 4 e no máximo 8.")

    def check_stars_connection(self, stars_count: int, star_a: int, star_b: int) -> str:
        """Verifica se duas estrelas, 'star_a' e 'star_b', estão diretamente interligadas."""
        # Verifica se a estrela está dentro das demais estrelas, de 0 à 'stars_count'.
        if not 0 <= star_a <= stars_count - 1 or not 0 <= star_b <= stars_count - 1:
            raise ValueError("Os índices das estrelas não estão corretos.")

        # Gera uma matriz aleatório, baseado no valor de 'star_a', e mostra-a.
        constellation = self._generate_constellation(stars_count, star_a)
        print(*constellation, sep="\n")

        # Mostra se há ligação ou não entre as estrelas 'star_a' e 'star_b'.
        if constellation[star_a][star_b] == constellation[star_b][star_a] == 1:
            return f'Há ligação entre as estrelas {star_a} e {star_b}.'
        return f'Não há ligação entre as estrelas {star_a} e {star_b}.'

if __name__ == "__main__":
    # Exemplo de entrada:
    # 5 2 3
    # Os índices começam a partir do 0.

    # Entrada única.
    print(Solution().check_stars_connection(*list(map(int, input("> ").split(" ")))))

    # Múltiplas entradas (Testcases).
    # for t in range(list(map(int, input("Testcases: ").split(" ")))[0]):
    #     print(Solution().check_stars_connection(*list(map(int, input("> ").split(" ")))))       
