from dataclasses import dataclass, field
from itertools import combinations
from random import sample, shuffle

# O valor de cada condição de vitória.
COMBINATIONS = {
    "Royal Flush": 10,
    "Straight Flush": 9,
    "Quadra": 8,
    "Full House": 7,
    "Flush": 6,
    "Straight": 5,
    "Trio": 4,
    "Dois Pares": 3,
    "Par": 2,
    "Carta Alta": 1
}

# Representa os valores das cartas "A", "J", "Q" e "K".
CONVERSION = {
    "A": 1,
    "J": 11,
    "Q": 12,
    "K": 13
}

@dataclass
class Deck:
    """Objeto responsável pelo baralho."""

    deck: list[str] = field(init=False, default_factory=list)

    def _raffle_cards(self) -> list[str]:
        """Sorteia cinco cartas aleatória à mesa."""
        # Pega cinco cartas aleatórias do baralho.
        cards = sample(self.deck, 5)
        # Remove as cartas do baralho.
        for card in cards:
            self.deck.remove(card)
        # Retorna as cartas sorteadas.
        return cards

    def __post_init__(self):
        """Gera um deck com 52 cartas, embaralhando-o ao final."""
        # Itera sobre os naipes das cartas.
        for suit in ["C", "E", "O", "P"]:
            # Itera sobre os valores das cartas.
            for number in ["A", *range(2, 11), "J", "Q", "K"]:
                self.deck.append(f'{number}{suit}')
        # Embaralha o deck.
        shuffle(self.deck)

@dataclass
class Table:
    """Representa a mesa."""

    cards: list[str] = field(init=False, default_factory=list)

    def _set_cards(self, new_set: list[str]):
        """Altera as cartas da mesa."""
        self.cards = new_set

    def show_cards(self) -> list[str]:
        """Mostra as cartas da mesa."""
        return self.cards

@dataclass
class Player:
    """Representa um jogador."""

    name: str = field(default_factory=str)
    deck: Deck = field(default_factory=Deck)
    table: Table = field(default_factory=Table)
    hand: list[str] = field(init=False, default_factory=list)
    win_condition: dict[str, list[bool | int]] = field(init=False, default_factory=dict)

    def _draw(self, deck: Deck):
        """Recebe duas cartas aleatórias do baralho."""
        # Pega duas cartas aleatórias do baralho.
        self.hand = sample(deck.deck, 2)
        # Remove as cartas do baralho.
        for card in self.hand:
            deck.deck.remove(card)

    def _check_royal_flush(self, sequency: list[str]) -> bool:
        """Verifica se há um 'Royal Flush'."""
        # Pega somente os valores das cartas, ignorando os naipes.
        cards_values = [card[:-1] for card in sequency]
        # Pega o naipe das cartas.
        cards_suits = set([card[-1] for card in sequency])
        # Verifica se há somente um naipe distinto.
        if len(cards_suits) == 1:
            # Verifica a ordem das cartas.
            if cards_values == ["A", "K", "Q", "J", "10"]:
                return True
        return False
    
    def _check_straight_flush(self, sequency: list[str]) -> bool:
        """Verifica se há um 'Straight Flush'."""
        # Pega os valores das cartas ignorando os naipes, converte-os para inteiros.
        cards_values = list(map(int, [CONVERSION[card[:-1]] if card[:-1] in CONVERSION else card[:-1] for card in sequency]))
        # Pega o naipe das cartas.
        cards_suits = set([card[-1] for card in sequency])
        # Verifica se há somente um naipe distinto.
        if len(cards_suits) == 1:
            # Verifica se está na ordem correta.
            if cards_values == sorted(cards_values) or cards_values == sorted(cards_values, reverse=True):
                return True
        return False

    def _check_quadra(self, sequency: list[str]) -> bool:
        """Verifica se há uma 'Quadra'."""
        # Pega somente os valores das cartas, ignorando os naipes. 
        cards_values = [card[:-1] for card in sequency]
        # Quantidade de cartas distintas, remove os duplicatas.
        card_count = set(cards_values)
        # Verifica se existe quatro cartas iguais.
        return any([cards_values.count(card) == 4 for card in card_count])
    
    def _check_full_house(self, sequency: list[str]) -> bool:
        """Verifica se há um 'Full House'."""
        # Pega somente os valores das cartas, ignorando os naipes.
        cards_values = [card[:-1] for card in sequency]
        # Quantidade de cartas distintas, remove os duplicatas.
        card_count = set(cards_values)
        # Verifica se existe duas cartas distintas, valores diferentes.
        if len(card_count) == 2:
            return True
        return False
    
    def _check_flush(self, sequency: list[str]) -> bool:
        """Verifica se há um 'Flush'."""
        # Pega os naipes das cartas, ignorando os valores.
        cards_suits = [card[-1] for card in sequency]
        # Quantidade de naipes distintos, remove os duplicatas.
        suit_count = set(cards_suits)
        # Verifica se existe um naipe.
        if len(suit_count) == 1:
            return True
        return False
    
    def _check_straight(self, sequency: list[str]) -> bool:
        """Verifica se há um 'Straight'."""
        # Pega os valores das cartas ignorando os naipes.
        cards_values = list(map(int, [CONVERSION[card[:-1]] if card[:-1] in CONVERSION else card[:-1] for card in sequency]))
        # Pega os naipes das cartas, ignorando os valores.
        cards_suits = [card[-1] for card in sequency]
        # Quantidade de naipes distintos, remove os duplicatas.
        suit_count = set(cards_suits)
        # Verifica se existe cinco naipes.
        if len(suit_count) == 4:
            # Verifica se está na ordem correta.
            if cards_values == sorted(cards_values) or cards_values == sorted(cards_values, reverse=True):
                return True
        return False
    
    def _check_trio(self, sequency: list[str]) -> bool:
        """Verifica se há um 'Trio'."""
        # Pega somente os valores das cartas, ignorando os naipes.
        cards_values = [card[:-1] for card in sequency]
        # Quantidade de cartas distintas, remove os duplicatas.
        card_count = set(cards_values)
        # Verifica se existe três cartas distintas, valores diferentes.
        if len(card_count) == 3:
            # Verifica se existe três cartas iguais.
            return any([cards_values.count(card) == 3 for card in card_count])
        return False
    
    def _check_dois_pares(self, sequency: list[str]) -> bool:
        """Verifica se há um 'Dois Pares'."""
        # Pega somente os valores das cartas, ignorando os naipes.
        cards_values = [card[:-1] for card in sequency]
        # Quantidade de cartas distintas, remove os duplicatas.
        card_count = set(cards_values)
        # Verifica se existe três cartas distintas, valores diferentes.
        if len(card_count) == 3:
            # Verifica e existe dois pares.
            return [cards_values.count(card) == 2 for card in card_count].count(True) == 2
        return False
    
    def _check_par(self, sequency: list[str]) -> bool:
        """Verifica se há um 'Par'."""
        # Pega somente os valores das cartas, ignorando os naipes.
        cards_values = [card[:-1] for card in sequency]
        # Quantidade de cartas distintas, remove os duplicatas.
        card_count = set(cards_values)
        # Verifica se existe quatro cartas distintas, valores diferentes.
        if len(card_count) == 4:
            # Verifica se existe um par.
            return any([cards_values.count(card) == 2 for card in card_count])
        return False

    def _check_carta_alta(self, sequency: list[str]) -> int:
        """Verifica se há um 'Carta Alta'."""
        # Pega somente os valores das cartas, ignorando os naipes.
        # Converte-os para inteiros e retorna o valor máximo.
        return max(list(map(int, [CONVERSION[card[:-1]] if card[:-1] in CONVERSION else card[:-1] for card in sequency])))

    def get_best_result(self) -> list:
        """Analiza a melhor vitória."""
        best_result = ["?", 0, "?"]
        # Itera sobre todas as vitórias de todas as combinações.
        for card in self.win_condition:
            # Para ao atingir a primeira vítoria das demais, valor hieraŕquico.
            for i, win in enumerate(self.win_condition[card]):
                if win:
                    # Tipo de vitória.
                    win_type = list(COMBINATIONS.keys())[i]
                    # Valor da vitória.
                    win_value = list(COMBINATIONS.values())[i]
                    if win_value > best_result[1]:
                        # Altera a condição de vitória, o valor e a mão.
                        best_result = [win_type, win_value, card]
                    break
        return best_result

    def check_cards(self) -> list:
        """Analiza as cartas da mão com as da mesa, alvejando a combinação de maior valor."""

        # Todas as possíveis condições de vitórias.
        every_win_condition = [
            self._check_royal_flush,
            self._check_straight_flush,
            self._check_quadra,
            self._check_full_house,
            self._check_flush,
            self._check_straight,
            self._check_trio,
            self._check_dois_pares,
            self._check_par,
            self._check_carta_alta
        ]

        # Itera sobre todas as combinações de três cartas da mesa.
        every_combination = list(combinations(self.table.show_cards(), 3))
        for sequency in every_combination:
            # Adiciona tal combinação à mão e verifica qual a condição de vitória.
            temporary_hand = self.hand + list(sequency)
            # Todas as condições de vitória para todas as combinações possíveis.
            self.win_condition[', '.join(str(_) for _ in temporary_hand)] = [condition(temporary_hand) for condition in every_win_condition]

        # Analisa a melhor vitória possível.
        return self.get_best_result()

    def __post_init__(self):
        self._draw(self.deck)

if __name__ == "__main__":
    # Inicializa o baralho.
    deck = Deck()
    # Inicializa a mesa.
    table = Table()
    # Inicializa os jogadores: 'Robô 1' e 'Robô 2', ambos com duas cartas na mão.
    robot_one = Player(name="Robô 1", deck=deck, table=table)
    robot_two = Player(name="Robô 2", deck=deck, table=table)
    # Adiciona cinco cartas, do baralho, à mesa.
    table._set_cards(deck._raffle_cards())
    # Faz cada jogador analisar a condição de vitória.
    robot_one_score = robot_one.check_cards()
    robot_two_score = robot_two.check_cards()
    # Anuncia o vencedor, a condição de vitória e suas cartas.
    # Empate.
    print(f"Robô 1: {robot_one_score[0]} ({robot_one_score[-1]}), Robô 2: {robot_two_score[0]} ({robot_two_score[-1]})")
    if robot_one_score[1] == robot_two_score[1]:
        print("Empatou!")
    # Robô 1 ganha.
    elif robot_one_score[1] > robot_two_score[1]:
        print("Robô 1 venceu!")
    # Robô 2 ganha.
    else:
        print("Robô 2 venceu!")